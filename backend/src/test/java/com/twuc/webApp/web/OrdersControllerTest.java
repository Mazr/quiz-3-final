package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.contract.CreateGoodsResponse;
import com.twuc.webApp.contract.CreateOrdersResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.Orders;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.List;

import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class OrdersControllerTest extends ApiTestBase {
    @Test
    void should_create_order_table() {
        
    }

    @Test
    void should_get_200() throws Exception {
        addThreeGoods();
        addOneOrder(new CreateGoodsResponse(1L,"北京陈霾", 8.8D, "口", "#"))
                .andExpect(status().is(200))
                .andExpect(header().string("location","/api/orders/1"));
    }

    void should_get_all_orders() throws Exception {
        List<CreateOrdersResponse> createOrdersResponses = getCreateOrdersResponses();
        addThreeOrders();
        mockMvc.perform(get("/api/orders"))
                .andExpect(status().is(201))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().json(serialize(createOrdersResponses)));
    }
}