package com.twuc.webApp.web;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.CreateGoodsResponse;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class GoodsControllerTest extends ApiTestBase {
    @Test
    void should_create_goods_table() {
    }

    @Test
    void should_get_201_status_code() throws Exception {
        addOneGoods("野生葫芦娃", 10D, "只", "#").andExpect(status().is(201));
    }

    @Test
    void should_get_all_goods() throws Exception {
        List<CreateGoodsResponse> goods = getCreateGoodsResponses();
        addThreeGoods();
        mockMvc.perform(get("/api/goods"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(content().json(serialize(goods)));
    }

    @Test
    void should_get_location_header() throws Exception {
        addOneGoods("野生葫芦娃", 10D, "只", "#")
                .andExpect(header().string("location", "/api/goods/1"));
    }

    @Test
    void should_get_goods_by_name() throws Exception {
        addDefaultGoods();
        mockMvc.perform(get("/api/goods/野生葫芦娃"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_400() throws Exception {
        addDefaultGoods();
        addOneGoods("野生葫芦娃", 10D, "只", "#")
                .andExpect(status().is(400));
    }

    @Test
    void should_return_200_when_I_delete_goods() throws Exception {
        addDefaultGoods();
        mockMvc.perform(get("/api/goods/野生葫芦娃"))
                .andExpect(status().is(200));
        mockMvc.perform(delete("/api/goods/1"))
                .andExpect(status().is(200));
        mockMvc.perform(get("/api/goods/野生葫芦娃"))
                .andExpect(status().is(400));
    }

}