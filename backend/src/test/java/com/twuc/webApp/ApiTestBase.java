package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.contract.CreateGoodsResponse;
import com.twuc.webApp.contract.CreateOrdersRequest;
import com.twuc.webApp.contract.CreateOrdersResponse;
import com.twuc.webApp.domain.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public abstract class ApiTestBase {
    @Autowired
    protected MockMvc mockMvc;
    private final ObjectMapper mapper;
    protected CreateGoodsResponse defaultGoods = new CreateGoodsResponse(1L,"北京陈霾", 8.8D, "口", "#");

    protected ApiTestBase() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    protected String serialize(Object obj) throws Exception {
        // 这个方法可以在测试中用来正确的将含有 Java 8 时间 field 的 Object 序列化为 JSON。
        return mapper.writeValueAsString(obj);
    }

    protected void addDefaultGoods() throws Exception {
        addOneGoods("野生葫芦娃", 10D, "只", "#");
    }

    protected List<CreateGoodsResponse> getCreateGoodsResponses() {
        List<CreateGoodsResponse> goods = new ArrayList<>();
        goods.add(new CreateGoodsResponse(1L, "北京陈霾", 8.8D, "口", "#"));
        goods.add(new CreateGoodsResponse(2L, "西安陈霾", 8.8D, "口", "#？"));
        goods.add(new CreateGoodsResponse(3L, "大草原新鲜空气", 888D, "口", "#"));
        return goods;
    }
    protected List<CreateOrdersResponse> getCreateOrdersResponses() {
        List<CreateOrdersResponse> orders = new ArrayList<>();
        orders.add(new CreateOrdersResponse(1L, defaultGoods));
        orders.add(new CreateOrdersResponse(2L, defaultGoods));
        orders.add(new CreateOrdersResponse(3L, defaultGoods));

        return orders;
    }

    protected ResultActions addOneGoods(String name, Double price, String unit, String prictureUrl) throws Exception {
        return mockMvc.perform(createGoods(name, price, unit, prictureUrl));
    }

    protected ResultActions addOneOrder(CreateGoodsResponse goods) throws Exception {
        return mockMvc.perform(createOrder(goods));
    }

    protected void addThreeGoods() throws Exception {
        addOneGoods("北京陈霾", 8.8D, "口", "#");
        addOneGoods("西安陈霾", 8.8D, "口", "#？");
        addOneGoods("大草原新鲜空气", 888D, "口", "#");
    }

    protected void addThreeOrders() throws Exception {
        addThreeGoods();
        addOneOrder(defaultGoods);
        addOneOrder(defaultGoods);
        addOneOrder(defaultGoods);
    }

    protected MockHttpServletRequestBuilder createGoods(String name, Double price, String unit, String pictureUrl) throws Exception {
        CreateGoodsRequest newGoods = new CreateGoodsRequest(name, price, unit, pictureUrl);
        return post("/api/goods")
                .contentType(APPLICATION_JSON_UTF8)
                .content(serialize(newGoods));
    }

    protected MockHttpServletRequestBuilder createOrder(CreateGoodsResponse goods) throws Exception {
        CreateOrdersRequest createOrdersRequest = new CreateOrdersRequest(goods);
        return post("/api/orders")
                .contentType(APPLICATION_JSON_UTF8)
                .content(serialize(createOrdersRequest));
    }
}
