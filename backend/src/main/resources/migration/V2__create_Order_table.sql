create table `orders`
(
    id       bigint auto_increment primary key,
    goods_id bigint,
    foreign key `fk_goods_name_con` (goods_id) references goods (id)
);