create table `goods`
(
    id   bigint auto_increment primary key,
    name VARCHAR(64) not null ,
    price DOUBLE not null ,
    unit VARCHAR(64) not null ,
    picture_url varchar(128) not null
)