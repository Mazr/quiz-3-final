package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Goods;

public class CreateOrdersRequest {
    private CreateGoodsResponse goods;

    public CreateOrdersRequest() {
    }

    public CreateOrdersRequest(CreateGoodsResponse goods) {
        this.goods = goods;
    }

    public CreateGoodsResponse getGoods() {
        return goods;
    }
}
