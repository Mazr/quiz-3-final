package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Goods;

public class CreateOrdersResponse {
    private Long id;
    private CreateGoodsResponse goods;


    public CreateOrdersResponse() {
    }

    public CreateOrdersResponse(Long id, CreateGoodsResponse goods) {
        this.id= id;
        this.goods = goods;
    }

    public Long getId() {
        return id;
    }

    public CreateGoodsResponse getGoods() {
        return goods;
    }
}
