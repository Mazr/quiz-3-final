package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateGoodsRequest {
    @NotNull
    @Size(max = 64)
    private String name;
    @NotNull
    private Double price;
    @NotNull
    @Size(max = 64)
    private String unit;
    @NotNull
    @Size(max = 64)
    private String pictureUrl;

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public CreateGoodsRequest() {
    }

    public CreateGoodsRequest(String name, Double price, String unit, String pictureUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureUrl = pictureUrl;
    }
}
