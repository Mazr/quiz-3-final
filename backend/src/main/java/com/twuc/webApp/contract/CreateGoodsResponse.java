package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Goods;

public class CreateGoodsResponse {
    private Long id;
    private String name;
    private Double price;
    private String unit;
    private String pictureUrl;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public CreateGoodsResponse(Long id, String name, Double price, String unit, String pictureUrl) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureUrl = pictureUrl;
    }
    public CreateGoodsResponse(Goods goods) {
        this.id = goods.getId();
        this.name = goods.getName();
        this.price = goods.getPrice();
        this.unit = goods.getUnit();
        this.pictureUrl = goods.getPictureUrl();
    }

    public CreateGoodsResponse() {
    }
}
