package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    @JoinColumn(name = "goods_id",referencedColumnName = "id")
    private Goods goods;

    public Orders(Goods goods) {
        this.goods = goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Long getId() {
        return id;
    }

    public Goods getGoods() {
        return goods;
    }

    public Orders() {
    }
}
