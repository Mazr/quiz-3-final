package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(length = 64,nullable = false)
    private Double price;
    @Column(length = 64,nullable = false)
    private String unit;
    @Column(length = 128,nullable = false)
    private String pictureUrl;

    @OneToOne(mappedBy = "goods")
    private Orders orders;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Goods() {
    }

    public Goods(String name, Double price, String unit, String pictureUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureUrl = pictureUrl;
    }
}
