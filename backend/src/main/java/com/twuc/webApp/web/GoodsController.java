package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.contract.CreateGoodsResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.service.GoodsService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping("/api/goods")
@Validated
public class GoodsController {
    private GoodsService goodsService;

    public GoodsController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @PostMapping
    ResponseEntity createGoods(@Valid @RequestBody CreateGoodsRequest createGoodsRequest) {
        if (goodsService.ensureNameExist(createGoodsRequest.getName())) {
            throw new IllegalArgumentException();
        }
        Goods goods = goodsService.addGoods(goodsService.createNewGoods(createGoodsRequest));
        CreateGoodsResponse goodsResponse = goodsService.getGoodsResponse(goods);
        return ResponseEntity.status(201)
                .header("location", "/api/goods/" + goodsResponse.getId())
                .build();
    }

    @GetMapping
    ResponseEntity getAllGoods() {
        List<Goods> allGoods = goodsService.getAllGoods();
        return ResponseEntity.status(200).contentType(APPLICATION_JSON_UTF8).body(allGoods);
    }

    @GetMapping("/{name}")
    ResponseEntity getGoods(@PathVariable String name) throws IllegalAccessException {
        CreateGoodsResponse goods = goodsService.getGoodsByName(name);
        return ResponseEntity.status(200).contentType(APPLICATION_JSON_UTF8).body(goods);
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteGoods(@PathVariable Long id) {
        goodsService.deleteGoodsById(id);
        return ResponseEntity.status(200).build();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity exceptionHandler(IllegalArgumentException e) {
        return ResponseEntity.status(400).build();
    }
}
