package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateOrdersRequest;
import com.twuc.webApp.domain.Orders;
import com.twuc.webApp.service.OrdersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {
    private OrdersService ordersService;

    public OrdersController(OrdersService ordersService) {
        this.ordersService = ordersService;
    }

    @PostMapping
    ResponseEntity createOrder(@RequestBody CreateOrdersRequest createOrdersRequest) {
        Orders newOrder = ordersService.createNewOrders(createOrdersRequest);
        Orders order = ordersService.createOrder(newOrder,createOrdersRequest.getGoods());
        return ResponseEntity.status(200)
                .header("location","/api/orders/" + order.getId()).build();
    }
    @GetMapping
    ResponseEntity getOrders() {
        List<Orders> allOrder = ordersService.getAllOrder();
        return ResponseEntity.status(201).contentType(APPLICATION_JSON_UTF8).body(allOrder);
    }
}
