package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateGoodsResponse;
import com.twuc.webApp.contract.CreateOrdersRequest;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.GoodsRepository;
import com.twuc.webApp.domain.Orders;
import com.twuc.webApp.domain.OrdersRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class OrdersService {
    private GoodsRepository goodsRepository;
    private OrdersRepository ordersRepository;
    private EntityManager em;

    public OrdersService(EntityManager em ,GoodsRepository goodsRepository, OrdersRepository ordersRepository) {
        this.goodsRepository = goodsRepository;
        this.ordersRepository = ordersRepository;
        this.em = em;
    }

    public Orders createOrder(Orders order, CreateGoodsResponse goods) {
        Orders savedOrder = ordersRepository.save(order);
        ordersRepository.flush();
        em.clear();
        return savedOrder;
    }

    public Orders createNewOrders(CreateOrdersRequest createOrdersRequest) {
        return new Orders(
                goodsRepository.findById(createOrdersRequest.getGoods().getId()).orElseThrow(IllegalArgumentException::new)
        );
    }

    public List<Orders> getAllOrder() {
        List<Orders> orders = ordersRepository.findAll();
        ordersRepository.flush();
        return orders;
    }
}
