package com.twuc.webApp.service;

import com.twuc.webApp.contract.CreateGoodsRequest;
import com.twuc.webApp.contract.CreateGoodsResponse;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.GoodsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsService {
    private GoodsRepository goodsRepository;

    public GoodsService(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    public Goods addGoods(Goods goods) {
        Goods savedGoods = goodsRepository.save(goods);
        goodsRepository.flush();
        return savedGoods;
    }

    public CreateGoodsResponse getGoodsResponse(Goods goods) {
        return new CreateGoodsResponse(goods);
    }

    public Goods createNewGoods(CreateGoodsRequest createGoodsRequest) {
        return new Goods(
                createGoodsRequest.getName(),
                createGoodsRequest.getPrice(),
                createGoodsRequest.getUnit(),
                createGoodsRequest.getPictureUrl()
        );
    }



    public List<Goods> getAllGoods() {
        return goodsRepository.findAll();
    }

    public CreateGoodsResponse getGoodsByName(String name){
        Goods goods = goodsRepository.findByName(name).orElseThrow(IllegalArgumentException::new);
        goodsRepository.flush();
        return getGoodsResponse(goods);
    }

    public boolean ensureNameExist(String name) {
        return goodsRepository.findByName(name).isPresent();
    }

    public void deleteGoodsById(Long id) {
        goodsRepository.deleteById(id);
        goodsRepository.flush();
    }
}
